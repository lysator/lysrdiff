#!/usr/bin/env python

# Copyright (C) 2007-2008  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import errno
import socket

def open_socket():
    s = socket.socket()
    try:
        s.connect(("127.0.0.1", 9933))
    except socket.error, e:
        if e[0] == errno.ECONNREFUSED:
            return None
        raise
    return s

def sendmsg(disk, part, msg):
    s = open_socket()
    if s is None:
	return
    s.send("set-status %d %d %s\n" % (disk, part, msg))
    s.close()

def completion(disk, part, category, subcategory, outcome):
    s = open_socket()
    if s is None:
	return
    s.send("completed %d %d %s %s %s\n" % (disk, part, 
					   category, subcategory, 
					   outcome))
    s.close()

if __name__ == '__main__':
    if sys.argv[1] == "--status":
        sendmsg(int(sys.argv[2]), int(sys.argv[3]), sys.argv[4])
    if sys.argv[1] == "--ok":
        completion(int(sys.argv[2]), int(sys.argv[3]), 
		   sys.argv[4], sys.argv[5], "OK")
    if sys.argv[1] == "--warn":
        completion(int(sys.argv[2]), int(sys.argv[3]), 
		   sys.argv[4], sys.argv[5], "WARN")
    if sys.argv[1] == "--fail":
        completion(int(sys.argv[2]), int(sys.argv[3]), 
		   sys.argv[4], sys.argv[5], "FAIL")
