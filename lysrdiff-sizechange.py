#!/usr/bin/env python

# Copyright (C) 2007  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

def root(disk, part):
    return "/lysrdiff/%d/perm/%d/lysrdiff" % (disk, part)

def taskfile(disk, part):
    return "%s/tasks" % root(disk, part)

def summary_file(disk, part, category, subcategory):
    return "%s/backups/%s/%s/backup-summary.txt" % (
        root(disk, part), category, subcategory)

def parse_summary(line):
    start_date, start_time, end_date, end_time, infotext = line.split(" ", 4)
    info = {}
    for item in infotext.split(" "):
        key, val = item.split("=", 1)
        info[key] = val
    return (start_date + " " + start_time,
            end_date + " " + end_time,
            info)

def report_sizechange(disk, part, category, subcategory):
    prev = 0
    curr = 0
    for line in open(summary_file(disk, part, category, subcategory)):
        start, end, info = parse_summary(line)
        sz = info.get("totalsize")
        if sz is not None:
            try:
                sz = int(sz)
                prev = curr
                curr = sz
            except ValueError:
                pass

    print curr - prev, category, subcategory


def report_partition_sizechange(disk, part):
    for line in open(taskfile(disk, part)):
        category, subcategory, server, origin = line.split(" ", 3)
        report_sizechange(disk, part, category, subcategory)

def main():
    for spec in sys.argv[1:]:
        disk, part = spec.split("/")
        report_partition_sizechange(int(disk), int(part))

if __name__ == '__main__':
    main()
