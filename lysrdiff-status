#!/bin/sh

# Copyright (C) 2006-2007, 2009  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CLONE=perm
#CLONE=B

df -hl /lysrdiff/*/$CLONE/*/lysrdiff
echo

statecache=/tmp/cached-state-$USER

for base in /lysrdiff/*/$CLONE/*/lysrdiff
do
  disk=`echo $base|sed 's%/lysrdiff/\([0-9]*\)/'$CLONE'/\([0-9]*\)/lysrdiff%\1%'`
  part=`echo $base|sed 's%/lysrdiff/\([0-9]*\)/'$CLONE'/\([0-9]*\)/lysrdiff%\2%'`
  # GNU findutils 4.3.3 and newer includes subsecond output in the %TS
  # format.  The sed statement truncates the seconds.
  find $base/state -type f \
      -printf "  %TY-%Tm-%Td %TH:%TM:%TS $disk/$part %P\n" \
      | sed 's/\([^:]*:[^:]*:[0-9][0-9]\)\.[0-9]*/\1/' \
      | sort > $statecache.$disk.$part.$$
  mv -f $statecache.$disk.$part.$$ $statecache-$disk.$part
  echo -n $disk/$part':'
  echo -n ' 'Tasks: `wc -l < $base/tasks`
  echo -n ' 'Fresh: `find $base/state -name \*--start -mtime 0 -print|wc -l`
  echo -n ' '1day: `find $base/state -name \*--start -mtime 1 -print|wc -l`
  echo -n ' 'Stale: `find $base/state -name \*--start -mtime +1 -print|wc -l`
  echo -n ' 'Tot: `ls $base/state/*--end|wc -l`
  echo -n ' 'Warn: `find $base/state -name \*--warn -print|wc -l`
  echo ' 'Err: `ls $base/state/*--fail 2>/dev/null|wc -l`
done
echo

echo Warnings:
echo
grep -h -- '--warn$' $statecache-* | sort
echo
echo Failures:
echo
grep -h -- '--fail$' $statecache-* | sort
echo
echo Top 5 most stale backups:
echo
grep -h -- '--start$' $statecache-* | sort | head -5
echo
echo Most stale per part:
echo
for i in $statecache-*
do
    grep -h -- '--start$' $i | sort | head -1
done
echo
echo Freshest per part:
echo
for i in $statecache-*
do
    grep -h -- '--start$' $i | sort | tail -1
done
echo
echo All unfinished backup attempts:
echo
grep -h -- '--attempt$' $statecache-* | sort | head -5
