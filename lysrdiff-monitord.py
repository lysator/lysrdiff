#!/usr/bin/env python

# Copyright (C) 2007  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import socket
import errno
import select
import sys
import time

def now():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))

class Client(object):
    def __init__(self, sock, peer, dispatcher):
        self.__s = sock
        self.__peer_ip = peer[0]
        self.__peer_port = peer[1]
        dispatcher.register(self)
        self.__writebuf = ""
        self._readbuf = ""
        self.__got_eof = False
        self.__dispatcher = dispatcher

    def fileno(self):
        return self.__s.fileno()

    def want_read(self):
        if self.__got_eof:
            return []
        else:
            return [self]

    def want_write(self):
        if self.__writebuf:
            return [self]
        else:
            return []

    def read_event(self):
        try:
            bytes = self.__s.recv(100)
        except socket.error, e:
            if e[0] == errno.ETIMEDOUT:
                self.got_eof(True)
                return
            raise
        if bytes == "":
            self.got_eof()
            return

        self._readbuf += bytes
        self.parse()

    def got_eof(self, error=False):
        self.__got_eof = True
        if not error:
            self.parse()
        if len(self._readbuf) > 0:
            sys.stderr.write("EOF with %d bytes pending from client.\n" % (
                len(self._readbuf)))
        if len(self.__writebuf) == 0 or error:
            self.__dispatcher.unregister(self)
            self.__s.close()
            del self

    def write_event(self):
	try:
            rv = self.__s.send(self.__writebuf)
        except socket.error, e:
	    if e[0] == errno.EAGAIN:
	        rv = 0
		# Give the rest of the system a chance to recover.
		time.sleep(0.05)
	    elif e[0] in [errno.ETIMEDOUT, errno.EHOSTUNREACH]:
		self.got_eof(True)
		return
	    else:
		raise
        if rv > 0:
            self.__writebuf = self.__writebuf[rv:]

    def parse(self):
        ix = self._readbuf.find("\n")
        if ix < 0:
            return
        cmd = self._readbuf[:ix]
        self._readbuf = self._readbuf[ix+1:]
        self.handle_cmd(cmd)

    def handle_cmd(self, cmd):
        if cmd == "":
            pass
        elif cmd.startswith("set-status "):
            split = cmd.split(None, 3)
            if len(split) == 3:
                cmd, disk, part = split
                status = ""
            else:
                cmd, disk, part, status = split
            dispatcher.set_status(int(disk), int(part), status)

        elif cmd.startswith("completed "):
            cmd, disk, part, category, subcategory, outcome = cmd.split()
	    dispatcher.completed(int(disk), int(part), category, subcategory, 
			         outcome)

        else:
            sys.stderr.write("Unknown command received from client.\n")

    def write(self, buf):
        self.__writebuf += buf
        self.write_event()

    def dispatcher(self):
	return self.__dispatcher


class Server(object):
    IP = '127.0.0.1'
    PORT = 9933
    CLIENT = Client

    def __init__(self, dispatcher):
        self.__s = socket.socket()
        self.__s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        self.__s.bind((self.IP, self.PORT))
        self.__s.listen(3)
        self.__s.setblocking(False)
        dispatcher.register(self)
        self.__dispatcher = dispatcher

    def want_read(self):
        return [self]

    def want_write(self):
        return []

    def fileno(self):
        return self.__s.fileno()

    def read_event(self):
	sock, peer = self.__s.accept()
	sock.setblocking(False)
        self.CLIENT(sock, peer, self.__dispatcher)

class Vt100Client(Client):

    def __init__(self, sock, peer, dispatcher):
        Client.__init__(self, sock, peer, dispatcher)
	self.write("\377\373\001") # IAC WILL ECHO
	self.write("\377\373\003") # IAC WILL SuppressGoahead
	self.write("\377\375\003") # IAC DO SuppressGoahead
	self.write("\033[7l")      # Disable line wrap.
	self.write("\033[r")	   # Scroll entire screen.
	self.write("\033[999;999H") # Move to row 999, col 999.
	self.write("\033[6n")	   # Report cursor position.
	self.rows = None
	self.cols = None

    def parse(self):
	print "Got data", repr(self._readbuf)
	while True:
            ix = self._readbuf.find("\033")
	    if ix < 0:
		print "Discarding"
	        self._readbuf = ""
	        return
	    m = re.match("\033\\[(?P<row>\\d+);(?P<col>\\d+)R", 
			 self._readbuf[ix:])
	    if m is None:
		print "ESC at", ix, "but discarding it"
		self._readbuf = self._readbuf[ix+1:]
	    else:
		break
	print "Got something nice"
	self.rows = int(m.group("row"))
	self.cols = int(m.group("col"))
	self._readbuf = self._readbuf[ix+1:]
	print "Got", self.rows, "rows and", self.cols, "cols"
	sys.stdout.flush()
        self.dispatcher().register_vt100(self)


class Vt100Server(Server):
    IP = ''
    PORT = 9934
    CLIENT = Vt100Client

class Dispatcher(object):
    def __init__(self):
        self.__clients = set()
        self.__vt100 = set()
        self.__break = False
        self.__status = {}

    def register(self, client):
        self.__clients.add(client)

    def unregister(self, client):
        self.__clients.remove(client)
        self.__vt100.discard(client)
        self.__break = True
        self.write_monitor_status()

    def toploop(self):
	prev_ts = now()
        while True:
            r = []
            w = []
            self.__break = False

            for c in self.__clients:
                r += c.want_read()
                w += c.want_write()

	    t = time.time()
	    maxwait = 1 - (t - int(t)) + 0.05
	    if maxwait < 0:
		maxwait = 0

            r, w, e = select.select(r, w, [], maxwait)

	    ts = now()

            for c in w:
                if self.__break:
                    break
                c.write_event()

            for c in r:
                if self.__break:
                    break
                c.read_event()

	    if ts != prev_ts:
		for c in list(self.__vt100):
		    c.write("\033[1;%dH%s\033[%d;1H" % (
			c.cols - len(ts), ts, c.rows - 1))

    def register_vt100(self, client):
        self.__vt100.add(client)

        msg = "\033[2J\033[HLYSrdiff status\r\n==============="
        for disk in self.__status.keys():
            for part in self.__status[disk].keys():
                msg += self.format_status_vt100(disk, part, client)
	msg += "\033[%d;%dr" % (9, client.rows - 2)
        client.write(msg)
        self.write_monitor_status()

    def write_monitor_status(self):
        for c in list(self.__vt100):
	    msg = self.format_monitor_status(c)
            c.write(msg)

    def format_monitor_status(self, client):
        msg = "\033[%d;1H" % client.rows
        msg += "%d clients" % len(self.__vt100)
        msg += "\033[K"
        msg += "\033[%d;1H" % (client.rows - 1)
        return msg

    def set_status(self, disk, part, status):
        if disk not in self.__status:
            self.__status[disk] = {}

        timestamp = now()
        self.__status[disk][part] = (timestamp, status)

        for c in list(self.__vt100):
	    msg = self.format_status_vt100(disk, part, c)
            c.write(msg)

        print timestamp, "%d/%d" % (disk, part), status

    def format_status_vt100(self, disk, part, client):
        timestamp, status = self.__status[disk][part]
        
        msg = "\033[%d;1H" % (2 + 2 * disk + part)
        msg += (timestamp + " " + status)[:client.cols]
        msg += "\033[K"
        msg += "\033[%d;1H" % (client.rows - 1)

        return msg

    def completed(self, disk, part, category, subcategory, outcome):
	msg = "%s %d/%d %s %s %s" % (now(), disk, part, outcome, 
				     category, subcategory)
	for c in list(self.__vt100):
	    c.write("\033[%d;1H\r\n%s\033[%d;1H" % (c.rows - 2,
						    msg,
						    c.rows - 1))

if __name__ == '__main__':
    dispatcher = Dispatcher()
    Server(dispatcher)
    Vt100Server(dispatcher)
    dispatcher.toploop()
