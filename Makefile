BINDIR=/opt/LYSrdiff/bin

all:;

install: install-one-task
	cp backup-all $(BINDIR)/
	cp backup-repeatedly $(BINDIR)/
	cp distribute-tasks $(BINDIR)/
	cp fetch-backup-work $(BINDIR)/
	cp fetch-work-pcfritz $(BINDIR)/
	cp lysrdiff-status $(BINDIR)/
	cp lysrdiff-df $(BINDIR)/
	cp lysrdiff-move-obsolete $(BINDIR)/
	cp lysrdiff-move-job $(BINDIR)/
	cp lysrdiff-clone-tasks $(BINDIR)/
	cp lysrdiff-move-many $(BINDIR)/
	cp lysrdiff-label-disk $(BINDIR)/
	cp lysrdiff-sizechange.py $(BINDIR)/
	cp lysrdiff-settings $(BINDIR)/
	cp update-origin-server $(BINDIR)/

install-one-task:
	cp backup-one-task $(BINDIR)/
	cp lysrdiff-set-status.py $(BINDIR)/
	cp lysrdiff-monitord.py $(BINDIR)/
