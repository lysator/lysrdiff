#!/bin/sh

# Copyright (C) 2008  Per Jonsson <poj@lysator.liu.se>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

usage () {
    echo $0: usage: $0 [ options ] disk part category oldserver newserver >&2
    echo Example: $0 0 1 lsvn stalingrad plantagenet >&2
    echo Recognized options: >&2
    echo ' --non-interactive Automatically commit changes' >&2
    echo 'Always performes changes to the "perm" disk.' >&2
}

interactive=1

while [ $# -gt 1 ]
do
  case "x$1" in
    x--non-interactive)
      shift
      interactive=0;;
    x--*)
      usage
      exit 1;;
    x*) break;;
  esac
done  

if [ $# != 5 ]
then
    usage
    exit 1
fi

disk="$1"
part="$2"
lysrdiffpart="$1/$2"
category="$3"
oldserver="$4"
newserver="$5"

category_dir="/lysrdiff/$disk/perm/$part/lysrdiff/backups/$category"

if [ ! -d $category_dir ]
then
    echo "$category not present on $lysrdiffpart" >&2
    exit 1
fi

cd $category_dir
for sc in *
do
    echo "------ $sc"
    OLD=$sc/origin
    NEW=$sc/origin.new
    cat $OLD | sed "s/^$oldserver/$newserver/" > $NEW
    diff -u $OLD $NEW 
    if [ $? != 0 ]
    then
	if [ $interactive = 1 ]
	then
	    echo -n '[CONFIRM] '
	    read line
	fi
	mv -f $NEW $OLD
    fi
done
