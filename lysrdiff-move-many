#!/bin/sh

# Copyright (C) 2008  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

usage () {
    echo $0: usage:
    echo "  $0 fromdisk/frompart todisk/topart size"
    echo
    echo Example:
    echo "  $0 1/0 2/1 12"
    echo would move 12 GB from /lysrdiff/1/perm/0 to /lysrdiff/2/perm/1.
    echo
    echo Please use lysrdiff-move-obsolete to fix the clones.
}

if [ $# -ne 3 ]
then
    usage >&2
    exit 1
fi

case "$1" in
    */*) ;;
    *) usage;
       exit 1;;
esac

case "$2" in
    */*) ;;
    *) usage;
       exit 1;;
esac

SRC_DISK=`echo $1 | sed 's%/.*%%'`
SRC_PART=`echo $1 | sed 's%.*/%%'`
DST_DISK=`echo $2 | sed 's%/.*%%'`
DST_PART=`echo $2 | sed 's%.*/%%'`
SIZE_GB=$3

SRC_ID=/lysrdiff/$SRC_DISK/perm/$SRC_PART/lysrdiff.id
DST_ID=/lysrdiff/$DST_DISK/perm/$DST_PART/lysrdiff.id

free_space () {
    stat -f -c '%f * %S' $1|xargs expr
}

if [ $SRC_ID = $DST_ID ]
then
    echo cannot move to self >&2
    exit 1
fi

DST_START_SIZE=`free_space $DST_ID`
GB=`expr 1024 '*' 1024 '*' 1024`
SIZE_BYTES=`expr $SIZE_GB '*' $GB`
DST_TARGET_SIZE=`expr $DST_START_SIZE - $SIZE_BYTES`

if [ $DST_TARGET_SIZE -lt $GB ]
then
    echo This move would leave less than 1 GB free space >&2
    exit 1
fi

while [ `free_space $DST_ID` -gt $DST_TARGET_SIZE ]
do
  lysrdiff-move-job $SRC_DISK/$SRC_PART $DST_DISK/$DST_PART || exit 1
done
echo Move complete.
echo
echo Please use lysrdiff-move-obsolete to clean up.
exit 0
