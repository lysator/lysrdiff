#!/bin/sh

# Copyright (C) 2007  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

uncloned=0

if [ x"$1" = x"--uncloned" ]
then
    uncloned=1
    shift
fi

if [ $# != 2 ]
then
    echo usage: "$0 [ --uncloned ] from to" >&2
    exit 2
fi

srcdisk=`echo $1|sed sx/.*xx`
srcpart=`echo $1|sed sx.*/xx`
dstdisk=`echo $2|sed sx/.*xx`
dstpart=`echo $2|sed sx.*/xx`

srctask=/lysrdiff/$srcdisk/perm/$srcpart/lysrdiff/tasks
dsttask=/lysrdiff/$dstdisk/perm/$dstpart/lysrdiff/tasks

mkdir $srctask.lock || exit 1
mkdir $dsttask.lock || {
	rmdir $srctask.lock
	exit 1
}

while true
do
    MOVEDTASK=/tmp/movedtask.$$
    head -1 $srctask > $MOVEDTASK || exit 1
    sed 1d $srctask > $srctask.new || exit 1
    cat $dsttask $MOVEDTASK > $dsttask.new || exit 1

    read category subcategory rest < $MOVEDTASK

    if [ $uncloned = 0 ]
    then
	break
    fi

    if [ -z "`ls /lysrdiff/$srcdisk/?/$srcpart/lysrdiff/backups/$category/$subcategory 2>/dev/null`" ]
    then
	break
    fi

    echo "Already cloned: $category $subcategory"
    cat $MOVEDTASK >> $srctask.new || exit 1
    mv $srctask.new $srctask
    rm $dsttask.new
    rm $MOVEDTASK
done

rm $MOVEDTASK

echo "Moving $category $subcategory"
ss=/opt/LYSrdiff/bin/lysrdiff-set-status.py
$ss $srcdisk $srcpart "Moving $category $subcategory to $dstdisk $dstpart"
$ss $dstdisk $dstpart "Moving $category $subcategory from $srcdisk $srcpart"

srcroot="/lysrdiff/$srcdisk/perm/$srcpart/lysrdiff"
dstroot="/lysrdiff/$dstdisk/perm/$dstpart/lysrdiff"
srcbase="$srcroot/backups/$category/$subcategory"
dstbase="$dstroot/backups/$category/$subcategory"
srcstate="$srcroot/state"
dststate="$dstroot/state"

if [ -d "$dstbase" ]
then
    echo "$dstbase": already exists >&2
    rmdir $srctask.lock
    rmdir $dsttask.lock
    exit 1
fi

if [ ! -d "$srcbase" ]
then
    echo "$srcbase": no such dir >&2
    rmdir $srctask.lock
    rmdir $dsttask.lock
    exit 1
fi

mkdir "$srcbase"/lock
mkdir -p "$dstbase"/lock
rsync -a "$srcbase/" "$dstbase" || exit 1
cp -a "$srcstate/$category--$subcategory--"* "$dststate" || exit 1
rmdir "$srcbase"/lock
rmdir "$dstbase"/lock

$ss $srcdisk $srcpart ""
$ss $dstdisk $dstpart ""

mv $srctask.new $srctask
mv $dsttask.new $dsttask
rmdir $srctask.lock
rmdir $dsttask.lock
